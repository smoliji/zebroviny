<?php

namespace Entities;

use Entities\Card;
use Symfony\Component\Yaml\Parser;

class CardDAO {

    const FILE_PATH = 'cards.csv';

    private static $instance = NULL;

    private $cards = array();
    private $categories = array();

    private function __construct(){
        $this->init();
    }

    /**
     *
     * @return CardDAO
     */
    public static function getInstance() {
        if (NULL === self::$instance){
            self::$instance = new CardDao();
        }
        return self::$instance;
    }

    private function init() {
        /*
        $file = fopen($this::FILE_PATH, 'r');
        if (!$file) { 
          throw new \Exception('Could not open file: "'.$this::FILE_PATH.'"');
          return false;
        }
        while (FALSE !== ($line = fgets($file))){
            $tokens = explode(',', $line);
            if (count($tokens) >= 3 ){
                if (!empty($tokens[0])) {
                    array_push(
                        $this->cards,
                        new Card($tokens[0], array(
                            Method::fromString($tokens[1]) => $tokens[2]
                        ))
                    );                  
                }
                else {
                    $this->cards[count($this->cards)-1]->addCategory(Category::fromString($tokens[1]), $tokens[2]);
                }
            }                      
        }
        return true;*/

        /*
        $parser = new Parser();
        $yml = $parser->parse(file_get_contents('cards.yml'));
        foreach ($yml as $word => $bucket){
            foreach ($bucket['categories'] as $cat) {
                if (!array_key_exists($cat, $this->categories)){
                    $this->categories[$cat] = 0;
                }
                $this->categories[$cat] += count($bucket['methods']);
            }
            foreach ($bucket['methods'] as $m => $points){
                $p = $points > 3 ? 3 : ($points < 1 ? 1 : $points);
                $card = new Card($word, $m, $p, $bucket['categories']);
                array_push($this->cards, $card);
            }

        }
        */

        $file = fopen($this::FILE_PATH, 'r');
        if (!$file) {
            throw new \Exception('Could not open file: "'.$this::FILE_PATH.'"');
            return false;
        }
        $header = fgets($file);
        while (FALSE !== ($line = fgets($file))){
            $tokens = explode(',', $line);
            $word = trim($tokens[0]);
            $draw = (int) trim($tokens[1]);
            $say = (int) trim($tokens[2]);
            $act = (int) trim($tokens[3]);
            $tags = array_filter(array_map('trim', array_slice($tokens, 4)));
            if ($word) {
                if ($draw) array_push($this->cards, new Card($word, Method::DRAW, $draw, $tags));
                if ($say) array_push($this->cards, new Card($word, Method::SAY, $say, $tags));
                if ($act) array_push($this->cards, new Card($word, Method::ACT, $act, $tags));
            }
            foreach ($tags as $tag) {
                if (!array_key_exists($tag, $this->categories)){
                    $this->categories[$tag] = 0;
                }
                $this->categories[$tag]++;
            }
        }
        fclose($file);
        return true;
    }


    /**
     *
     * @return array of Card
     */
    public function findAll() {
        return $this->cards;
    }

    /**
     *
     * @param string $category
     * @return array of Card
     */
    public function findAllFromCategory($category) {
        $cards = array();
        foreach ($this->cards as $card){
            if ($card->isCategory($category)){
                array_push($cards, $card);
            }
        }
        return $cards;
    }

    /**
     *
     * @param array $categories
     * @return array of Card
     */
    public function findAllFromCategoriesByName(array $categories) {
        $cards = array();
        foreach ($this->cards as $card){
            if (count(array_intersect($card->getCategories(), $categories))){
                array_push($cards, $card);
            }
        }
        return $cards;
    }

    /**
     *
     * @return Card
     */
    public function pickRandom() {
        return $this->cards[ rand(0, count($this->cards)-1) ];
    }

    /**
     *
     * @return array of string
     */
    public function getCategories() {
        return $this->categories;
    }

    /**
     * @return array
     */
    public function pickAllFullCards(){
        $output = array();
        $words = array();

        foreach($this->cards as $card)
            if(!in_array($card->getWord_Phrase(), $words, true))
                array_push($words, $card->getWord_Phrase());

        foreach($words as $word){
            $tri = array();
            $c = array();
            foreach($this->cards as $card){
                if($card->getWord_Phrase() == $word){
                    $tri[$card->getMethod()] = $card->getValue();
                    $c["word_phrase"] = $card->getWord_Phrase();
                    $c["categories"] = $card->getCategories();
                }
            }
            $c["methods"] = $tri;
            array_push($output, $c);
        }
        return $output;
    }

}