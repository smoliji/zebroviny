<?php
/**
 * Created by PhpStorm.
 * User: smolijar
 * Date: 2/16/15
 * Time: 7:48 PM
 */

namespace Entities;


class ColorPalette {

    private static $currIdx = 0;
    private static $colors = [
        "crimson",
        "goldenRod",
        "navajoWhite",
        "seaGreen",
        "deepSkyBlue",
        "darkOrchid",
        "turquoise",
        "darkSlateBlue",
    ];

    /**
     * @return string Random color
     */
    public function random(){
        return self::$colors[ array_rand(self::$colors) ];
    }

    /**
     * @return array of randomized colors
     */
    public function randomized(){
        $ret = self::$colors;
        shuffle($ret);
        return $ret;
    }

    /**
     * Shuffles colors array
     */
    public function randomize() {
        shuffle(self::$colors);
    }

    /**
     * @return string Next color.
     */
    public function next() {
        $o = self::$colors[ self::$currIdx ];
        self::$currIdx = ++self::$currIdx % count(self::$colors);
        return $o;
    }
} 