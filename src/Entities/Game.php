<?php

namespace Entities;

use Entities\Deck;

class Game implements \JsonSerializable {

    protected $name = null;
    protected $deck = null;
    protected $discard = null;
    protected $colors = array();
    protected $names = array();
    
    public function __construct($name = '') {
        if (empty($name)) {
            $this->name = date_format(new \DateTime("now"), "Ymd-His");
        } else {
            $this->name = $name;
        }

        $cardDao = CardDAO::getInstance();
        $cards = $cardDao->findAll();
        $this->deck = new Deck();  
        $this->discard = new Deck();

        // names
        $this->names = [
            "Acorn",
            "Acrobat",
            "Argentine",
            "Armadillo",
            "Army",
            "Atta",
            "Big Head",
            "Bull",
            "Bullet",
            "Carpenter",
            "Crazy",
            "Dracula",
            "Driver",
            "Fire",
            "Gliding",
            "Honey",
            "Jack Jumper",
            "Leaf Cutter",
            "Lemon",
            "Little Black",
            "Mealy",
            "Pharaoh",
            "Rover",
            "Slave Maker",
            "Sri Lankan",
            "Thief",
            "Trap Jaw",
            "Weaver",
            "Western Harvester",
            "Yellow Citronella",
        ];
        shuffle($this->colors);
        shuffle($this->names);
    }
    
    public function getDeck() {
        return $this->deck;
    }
    
    public function setDeck(Deck $deck) {
        $this->deck = $deck;
    }
    
    public function getDiscard() {
        return $this->discard;
    }
    
    public function getName() {
        return $this->name;
    }
    
    public function setName($name) {
        $this->name = $name;
    }
    
    public function draw() {
        if ($this->deck->count()) {
            $this->discard->addCard($this->deck->draw());
        }
    }
    
    public function drawback() {
        if ($this->discard->count()) {
            $this->deck->addCard($this->discard->draw());
        }
    }

    /**
     * @param $index
     * @return mixed
     */
    public function getNames($index)
    {
        return $this->names[$index];
    }
    
    public function __toString() {
        ob_start();
        echo $this->name;
        echo 'Deck: '.$this->deck;
        echo 'Discard:'.$this->discard;       
        $return = ob_get_contents();
        ob_end_clean();
        return $return;
    }

    public function jsonSerialize() {
        return [
            'name' => $this->name,
            'deck' => $this->deck,
            'discard' => $this->discard,
        ];
    }

}

?>