<?php

namespace Entities;

abstract class Method {

    const SAY = 'say';
    const ACT = 'act';
    const DRAW = 'draw';
    
    public static function fromString($s){
        
        switch (trim(strtolower($s))) {
            case 'say': return Method::SAY;
            case 'act': return Method::ACT;
            case 'draw': return Method::DRAW;
            default: return FALSE;        
        }
    }

}
