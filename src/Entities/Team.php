<?php

namespace Entities;

use Entities\Player;

class Team implements \JsonSerializable {
    
    protected $name;
    protected $players = array();
    
    public function __construct($name = 'Unknown') {
        $this->name = $name;
    }
    
    public function size() {
        return count($this->players);
    }
    
    public function addPlayer(Player $p) {
        array_push($this->players, $p);
    }
    
    public function removePlayer(Player $p) {
        foreach ($this->players as $pos => $player) {
            if ($player->equals($p)) {
                unset($this->players[$pos]);
            }
        }
        $this->players = array_values($this->players);        
    }
    
    public function removePlayerByName($playerName) {
        foreach ($this->players as $pos => $player) {
            if ($player->getName() === $playerName) {
                unset($this->players[$pos]);
            }
        }
        $this->players = array_values($this->players);        
    }
    
    /**
     * 
     * @param \Entities\Player $p
     * @return Player
     */
    public function getPlayer(Player $p) {
        foreach ($this->players as $player) {
            if ($player->equals($p)) {
                return $player;
            }
        }
        return null;
    }
    
    public function getPlayers() {
        return $this->players;
    }
    
    /**
     * 
     * @param type $playerName
     * @return Player
     */
    public function getPlayerByName($playerName) {
        foreach ($this->players as $player) {
            if ($player->getName() === $playerName) {
                return $player;
            }
        }
    }
    
    public function getName() {
        return $this->name;
    }

    public function setName($name) {
        $this->name = $name;
    }
    
    public function contains(Player $p) {
        foreach ($this->players as $player) {
            if ($player->equals($p)) {
                return true;
            }
        }
        return false;
    }
    
    public function jsonSerialize() {
        return [
            'name' => $this->name,
            'players' => $this->players,
        ];
    }
    
}
