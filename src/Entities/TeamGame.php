<?php

namespace Entities;

use Entities\FreeGame;
use Entities\Team;

class TeamGame extends FreeGame implements \JsonSerializable{
    
    protected $teams = array();
    protected $scores = array();
    
    public function addTeam(Team $t) {
        $this->teams[$t->getName()] = $t;
        $this->scores[$t->getName()] = 0;
    }

    public function removeTeam(Team $t) {
        unset($this->teams[$t->getName()]);
        unset($this->scores[$t->getName()]);
    }
    
    public function setTeamScore($teamName, $score) {
        if (!array_key_exists($this->teams, $teamName)) return false;
        $this->scores[$teamName] = $score;
    }
    
    /**
     * 
     * @param type $i
     * @return Team
     */
    public function getTeam($i) {
        if (array_key_exists($i, $this->teams)) {
            return $this->teams[$i];
        }
        return null;
    }
    
    public function getTeams() {
        return $this->teams;
    }
    
    public function getScores() {
        return $this->scores;
    }
    
    /**
     * 
     * @param type $name
     * @return Team
     */
    public function getTeamByName($name) {
        foreach ($this->teams as &$team) {
            if ($team->getName() === $name) {
                return $team;
            }
        }
        return null;
    }
    
    public function jsonSerialize() {
        return [
            'name' => $this->name,
            'teams' => $this->teams,
            'scores' => $this->scores,
            'setTeamScore' => 'function(teamName, newScore){for (var i = 0; i < this.teams.length; i++){var team = this.teams[i]; if (team.name === teamName){team.score = newScore;} } }'
        ];
    }
    
    
}
