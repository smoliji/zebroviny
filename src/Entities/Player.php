<?php

namespace Entities;

class Player implements \JsonSerializable{
    
    protected $name = null;
    
    /**
     * 
     * @param type $name
     */
    public function __construct($name = 'Unknown') {
        $this->name = $name;
    }
    
    public function getName() {
        return $this->name;
    }

    public function setName($name) {
        $this->name = $name;
    }
    
    public function equals(Player $p) {
        return $this->name === $p->name;
    }
    
    public function jsonSerialize() {
        return [
            'name' => $this->name,
        ];
    }

}
