<?php

namespace Entities;

class Deck {

    /** @var Card[]  */
    private $cards = array();
    
    public function __construct(array $cards = []) {  
        $this->cards = $cards;
    }

    /**
     * @return array|Card[]
     */
    public function getCards() {
        return $this->cards;
    }

    public function setCards(array $cards) {
        $this->cards = $cards;        
    }

    /**
     * Adds a card at the end of the deck.
     * @param $card Card
     */
    public function addCard($card) {
        array_push($this->cards, $card);
    }

    /**
     * Shuffles the deck's Card-s.
     */
    public function shuffle() {
        shuffle($this->cards);
    }

    /**
     * Draws (and removes) Card on top of the deck.
     * @return Card
     */
    public function draw() {
        return array_shift($this->cards);
    }

    public function getName() {
        return $this->name;
    }

    /**
     * Returns number of Card-s in deck.
     * @return int
     */
    public function count() {
        return count($this->cards);
    }

    /**
     * Returns (but not removes) top $count cards from top of the deck.
     * @param $count How many cards to return.
     * @return array Array of Entity\Card
     */
    public function top($count) {
        $o = array();
        for ($i = 0; $i < min($this->count(), $count); $i++) {
            $o[$i] = $this->cards[$i];
        }
        return $o;
    }

    /**
     * Removes duplicities in word_phrases. 
     */
    public function uniq() {
        $uniq = [];
        foreach ($this->cards as $card) {
            if (isset($uniq[$card->getWord_Phrase()])) {
                continue;
            }
            $uniq[$card->getWord_Phrase()] = $card;
        }
        $this->cards = array_values($uniq);
    }

    public function __toString() {
        ob_start();
        foreach($this->cards as $key => $card)
          echo '['.$key.']'.print_r($card, TRUE);        
        $return = ob_get_contents();
        ob_end_clean();
        return $return;
    }
    

}

?>