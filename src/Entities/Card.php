<?php

namespace Entities;

class Card implements \JsonSerializable {

    private $word_phrase;
    private $method;
    private $value;
    private $categories = array();

    /**
     * @param $wp Word phrase
     * @param $m Entities\Method Method
     * @param $v Value
     * @param array $categories
     */
    function __construct ($wp, $m, $v, array $categories = []){
        $this->word_phrase = $wp;
        $this->method = $m;
        $this->value = $v;
        $this->categories = $categories;     
    }
    
    public function addCategory($cat, $value){
        $this->categories[$cat] = $value;
    }
    
    public function isCategory($cat) {
        return in_array($cat, $this->categories);
    }
    
    public function getCategories() {
        return $this->categories;
    }
    
    public function setCategories($cats) {
        $this->categories = $cats;
    }
    
    public function getWord_Phrase() {
        return $this->word_phrase;
    }
    
    public function getMethod() {
        return $this->method;
    }
    
    public function getValue() {
        return $this->value;
    }
    
    function __toString(){
        return $this->word_phrase;
    }

    public function jsonSerialize() {
        return [
            'word_phrase' => $this->word_phrase,
            'method' => $this->method,
            'value' => $this->value,
            'categories' => $this->categories
        ];
    }

}