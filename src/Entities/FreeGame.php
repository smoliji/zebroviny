<?php

namespace Entities;

use Entities\Game;

class FreeGame extends Game {
    
    private $categories = array();
    private $cardsCount = 0;
    private $countersCount = 0;
    
    public function addCategories($cat) {
        array_push($this->categories, $cat);
    }
    
    public function setCategories($cat) {
        $this->categories = $cat;
    }
    
    public function getCategories() {
        return $this->categories;
    }
    
    public function getCardsCount() {
        return $this->cardsCount;
    }
    
    public function setCardsCount($count) {
        $this->cardsCount = $count;
    }
    
    public function getCountersCount() {
        return $this->countersCount;
    }
    
    public function setCountersCount ($count) {
        $this->countersCount = $count;
    }

    public function getDescription() {
        return 'This is a free game description';
    }    
    
    public function jsonSerialize() {
        return [
            'name' => $this->name,
            'categories' => $this->categories,
            'cardsCount' => $this->cardsCount,
            'deck' => $this->deck,
            'discard' => $this->discard,            
        ];
    }
    
}

