<?php

namespace Forms;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class TeamType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text', array(
                'label' => false,
            ))
            ->add('players', 'collection', array(
                'type' => new PlayerType(),
                'label' => false,
                'allow_add' => true,
                'prototype_name' => '__player_prot__',
                'options' => array(
                    'label' => false,
                    ),
            ));
    }


    public function getName()
    {
        return 'group';
    }

    public function setDefaultOptions( \Symfony\Component\OptionsResolver\OptionsResolverInterface $resolver)
    {
            $resolver->setDefaults(array(
                'data_class' => 'Entities\Team',
            ));
        }
}