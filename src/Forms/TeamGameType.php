<?php

namespace Forms;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Entities\CardDAO;
use Forms\TeamType;

class TeamGameType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $categories = CardDAO::getInstance()->getCategories();
        $categories = array_combine(array_keys($categories), array_keys($categories));
        (new \Collator('cs_CZ'))->asort($categories);

        $builder
            ->add('cardsCount', 'integer', array(
                'label'=>'Počet karet v balíku',
                'data' => 10,
                'constraints' => new Assert\Range(array(
                    'min'=>1,
                    'minMessage'=>'Počet karet musí být větší nebo roven 1.',
                    'max' => 45,
                    'maxMessage' => 'Počet karet musí být menší nebo roven 45.'
                )),
                'error_bubbling' => true,
            ))
            ->add('allCat', 'choice', array(
                'expanded' => true,
                'choices' => [0 => "Vše", 1 => 'Vybrat...'],
                'mapped' => false,
                'label' => false,
                'data' => 0,
            ))
            ->add('categories', 'choice', array(
                    'choices' => $categories,
                    'expanded' => true,
                    'multiple' => true,
                    'constraints' => new Assert\Count(array('min' => 1, 'minMessage' => 'Vyberte nejméně jednu kategorii')),
                    'error_bubbling' => true,
                )
            )
            ->add('teams', 'collection', array(
                'type' => new TeamType(),
                'allow_add' => true,
                'by_reference' => false,
                'prototype_name' => '__team_prot__',
                'options' => array(
                    'label' => false,
                    ),
            ))
            ->add('createGame', 'submit', array('label'=>'Hrát'));
    }


    public function getName()
    {
        return 'teamGame';
    }

    public function setDefaultOptions( \Symfony\Component\OptionsResolver\OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Entities\TeamGame',
        ));
    }
}