<?php
/**
 * Created by PhpStorm.
 * User: Jirka
 * Date: 20.1.15
 * Time: 9:52
 */

namespace Forms;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Entities\CardDAO;

class FreeGameType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $categories = CardDAO::getInstance()->getCategories();
        $categories = array_combine(array_keys($categories), array_keys($categories));
        (new \Collator('cs_CZ'))->asort($categories);

        $builder->add('cardsCount', 'integer', array(
            'label' => 'Počet karet v balíku',
            'data' => 10,
            'constraints' => new Assert\Range(array(
                'min'=>1,
                'minMessage'=>'Počet karet musí být větší nebo roven 1.',
                'max' => 45,
                'maxMessage' => 'Počet karet musí být menší nebo roven 45.'
            )),
            'error_bubbling' => true,
        ))
            ->add('allCat', 'choice', array(
                'expanded' => true,
                'choices' => [0 => "Vše", 1 => 'Vybrat...'],
                'mapped' => false,
                'label' => false,
                'data' => 0,
            ))
            ->add('countersCount', 'integer', array(
                'label' => 'Počet počítadel',
                'error_bubbling' => true,
            ))
            ->add('categories', 'choice', array(
                    'choices' => $categories,
                    'expanded' => true,
                    'multiple' => true,
                    'constraints' => new Assert\Count(array('min' => 1, 'minMessage' => 'Vyberte nejméně jednu kategorii')),
                    'error_bubbling' => true,
                )
            )
            ->add('createGame', 'submit', array('label' => 'Hrát'));
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return "freeGame";
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Entities\FreeGame',
        ));
    }


} 