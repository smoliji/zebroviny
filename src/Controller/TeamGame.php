<?php

use Entities\TeamGame;
use Entities\Team;
use Entities\Player;
use Entities\ColorPalette;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Entities\CardDAO;
use Entities\Deck;
use Forms\TeamGameType;
use Symfony\Component\Validator\Constraints as Assert;

$teamgame = $app['controllers_factory'];

$teamgame->match('/team-game', function(Request $request) use ($app) {
    $cardDao = CardDAO::getInstance();
    $categories = $cardDao->getCategories();

    $data = $request->request;

    //SF form
    $game = new TeamGame();
    $form = $app['form.factory']->createNamedBuilder('teamGameOptions', new TeamGameType(), $game)->getForm();
    
    $form->handleRequest($request);

    if ($request->isMethod('POST')) {
        if (!$form->isValid()) {
            if ($request->isXmlHttpRequest()) {
                $errF = $app['form.getAllErrors'];
                return new Response(json_encode($errF($form)), 200, array('Content-Type'=>'application/json'));
            }
        }
        else {
            $cardDao = CardDAO::getInstance();
            $tg = $form->getData();

            $allCat = $form->get('allCat')->getData();

            // Mapping categories. Will be removed when Doctrine arrives..if ever.
            $deckWithSpecifiedCats = new Deck($cardDao->findAllFromCategoriesByName($tg->getCategories()));
            //get specified amount of cards
            $deckWithSpecifiedCats->shuffle();
            $deckWithSpecifiedCats->uniq();
            $tg->setDeck(new Deck($deckWithSpecifiedCats->top($tg->getCardsCount())));

            //Forward
            $subRequest = Request::create($app['url_generator']->generate('games_team_game_play'), 'GET', array('game'=>$tg), $request->cookies->all(), array(), $request->server->all());
            return $app->handle($subRequest, HttpKernelInterface::SUB_REQUEST);
        }
    }

    return $app['twig']->render('TeamGame/teamGameOptions.html.twig', array(
       'categories' => $categories,
       'data' => $data,
       'form' => $form->createView()
    ));
})->bind('games_team_game');

$teamgame->match('/team-game/play', function(Request $request) use ($app) {
    $game = $request->query->get('game');
    $palette = new ColorPalette();
    return $app['twig']->render('TeamGame/game.html.twig', array(
        'game'=>$game,
        'jsonGame' => json_encode($game),
        'colorPalette' => $palette,
    ));
})->bind('games_team_game_play');

return $teamgame;