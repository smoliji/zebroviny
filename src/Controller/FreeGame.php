<?php

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Entities\CardDAO;
use Entities\FreeGame;
use Entities\Deck;
use Entities\ColorPalette;
use Forms\FreeGameType;
use Symfony\Component\Validator\Constraints as Assert;

$freegame = $app['controllers_factory'];

$freegame->match('/free-game', function (Request $request) use ($app) {
    $cardDao = CardDAO::getInstance();
    $categories = $cardDao->getCategories();

    $data = $request->request;

    //SF form
    $game = new FreeGame();
    $form = $app['form.factory']->createNamedBuilder('freeGameOptions', new FreeGameType(), $game)->getForm();

    $form->handleRequest($request);

    if ($request->isMethod('POST')) {
        if (!$form->isValid()) {
            if ($request->isXmlHttpRequest()) {
                $errF = $app['form.getAllErrors'];
                return new Response(json_encode($errF($form)), 200, array('Content-Type' => 'application/json'));
            }
        } else {
            $cardDao = CardDAO::getInstance();
            $fg = $form->getData();

            // Mapping categories. Will be removed when Doctrine arrives..if ever.
            $deckWithSpecifiedCats = new Deck($cardDao->findAllFromCategoriesByName($fg->getCategories()));
            //get specified amount of cards
            $deckWithSpecifiedCats->shuffle();
            $deckWithSpecifiedCats->uniq();
            $fg->setDeck(new Deck($deckWithSpecifiedCats->top($fg->getCardsCount())));

            //Forward
            $subRequest = Request::create($app['url_generator']->generate('games_free_game_play'), 'GET', array('game' => $fg), $request->cookies->all(), array(), $request->server->all());
            return $app->handle($subRequest, HttpKernelInterface::SUB_REQUEST);
        }
    }



    return $app['twig']->render('FreeGame/freeGameOptions.html.twig', array(
        'categories' => $categories,
        'data' => $data,
        'form' => $form->createView()));
})->bind('games_free_game');

$freegame->match('/free-game/play', function (Request $request) use ($app) {
    //colors
    $palette = new ColorPalette();
    $palette->randomize();

    $game = $request->query->get('game');
    return $app['twig']->render('FreeGame/game.html.twig', array(
        'game' => $game,
        'colorPalette' => $palette,
    ));
})->bind('games_free_game_play');

return $freegame;