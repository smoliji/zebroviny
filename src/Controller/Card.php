<?php

use Symfony\Component\HttpFoundation\Response;
use Entities\CardDAO;

$cards = $app['controllers_factory'];

$cards->match('/list', function() use ($app) {
    $dao = CardDAO::getInstance();
    return new Response(json_encode($dao->findAll()), 200, array(
        'Content-Type'=>'application/json', 'Access-Control-Allow-Origin'=>'*'));
})->bind('cards_list');

$cards->match('/random', function() use ($app) {
    $dao = CardDAO::getInstance();
    return new Response(json_encode($dao->pickRandom()), 200, array(
        'Content-Type'=>'application/json', 'Access-Control-Allow-Origin'=>'*'));
})->bind('cards_random');

$cards->match('/categories', function() use ($app) {
    $dao = CardDAO::getInstance();
    return new Response(json_encode($dao->getCategories()), 200, array(
        'Content-Type'=>'application/json', 'Access-Control-Allow-Origin'=>'*'));
})->bind('cards_categories');

$cards->post('/report', function(\Symfony\Component\HttpFoundation\Request $request) {

    $data = $request->request->all();

    if (!isset($data['word'], $data['method'], $data['value'], $data['comment'])){
        return new Response(json_encode($_POST), 400);
    }

    $card = new \Entities\Card($data['word'], $data['method'], $data['value']);
    $comment = $data['comment'];

    $content = sprintf("%s;%s;%d;%s%s",
        $card->getWord_Phrase(),
        $card->getMethod(),
        $card->getValue(),
        $comment,
        PHP_EOL
    );

    file_put_contents('CardComments.log', $content, FILE_APPEND);

    return new Response(null, 201);

})->bind('cards_report');

return $cards;

