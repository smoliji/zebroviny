<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\HttpKernelInterface;

$games = $app['controllers_factory'];

$games->match('/infinite-card', function() use ($app) {
   
    $subRequest = Request::create('/cards/random', 'GET');
    $response = $app->handle($subRequest, HttpKernelInterface::SUB_REQUEST);     
    $response = $response->getContent();
    
    $card = json_decode($response);
        
    return $app['twig']->render('InfiniteCard/infiniteCard.html.twig', 
            array('card' => $card));
    
})->bind('games_infinite_card');



return $games;

