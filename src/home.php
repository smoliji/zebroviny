<?php

$home = $app['controllers_factory'];

$home->match('/', function() use ($app) {
    return $app['twig']->render('index.html.twig');
})->bind('home');

$home->match('/browse-cards', function() use ($app) {
    $cardDao = Entities\CardDAO::getInstance();
    $cards = $cardDao->pickAllFullCards();
    return $app['twig']->render('browse.html.twig', array('cards'=>$cards));
})->bind('browse_cards');

return $home;

