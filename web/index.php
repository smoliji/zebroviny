<?php

$loader = require_once __DIR__.'/../vendor/autoload.php';

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\Yaml\Parser;
use Silex\Application\UrlGeneratorTrait;
use Silex\Provider\FormServiceProvider;
use Silex\Application;

$app = new Silex\Application();

$app['debug'] = true;



$app->register(new Silex\Provider\UrlGeneratorServiceProvider());
$app->register(new Silex\Provider\SessionServiceProvider());
$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => __DIR__.'/views',
    'twig.form.templates' => array('Form'.DIRECTORY_SEPARATOR.'form_bootstrap_template.twig'),
));
$app->register(new FormServiceProvider());
$app->register(new Silex\Provider\ValidatorServiceProvider());
$app->register(new Silex\Provider\TranslationServiceProvider(), array(
    'translator.messages' => array(),
));

$app['form.getAllErrors'] = $app->protect(function (\Symfony\Component\Form\Form $form) use(&$app) {
    $errors = array();
    $f = $app['form.getAllErrors'];
    foreach ($form->getErrors() as $key => $error) {
            $errors[$key] = $error->getMessage();
    }
    foreach ($form->all() as $child) {
        if (!$child->isValid()) {
            //$label = $child->getConfig()->getOptions()['label'];
            $errors[$child->getName()] = $f($child);
        }
    }

    return $errors;
});

/*$app['twig']->addFunction(new \Twig_SimpleFunction('path', function($url) use ($app) {
    return $app['url_generator']->generate($url);
}));*/
                                
/*
$slugConverter = function($s) {
  return $s.'Converted';
};                                   

$app->get('/hello/{slug}', function($slug) {
    $msg = $slug;
    throw new \Exception();
    return new Response('<h1>Foo: '.$msg.'</h1>');
})->convert('slug', $slugConverter) //data preproces. func.
  ->value('slug', 'DefaultSlug')    //default slug value
  ->bind('hello_route')             //route name
  //->assert('slug', '\d+')  for numeric slug only restriction
  ;
                                       
$app->get('/', function() {
    return new Response('foo');
});

$app->get('/redirect', function() use ($app) {
    return $app->redirect('/');
});

$app->get('/forward', function() use ($app) {
   $subRequest = Request::create('/foo', 'GET');
   return $app->handle($subRequest, HttpKernelInterface::SUB_REQUEST);
});*/
/*
class TestController {
    private $app;
    public function __construct($app) {
        $this->app = $app;
        
        $app->get('hello', array($this, 'helloAction'));
    }
    public function helloAction() {
        throw new \Exception();
        return new Response('helloAction', 200);
    }
}*/

$loader->add('Entities', __DIR__.'/../src/');
$loader->add('Forms', __DIR__.'/../src/');

$app->mount('/', include './../src/home.php');
$app->mount('/cards', include './../src/Controller/Card.php');
$app->mount('/games', include './../src/Controller/InfiniteCard.php');
$app->mount('/games', include './../src/Controller/FreeGame.php');
$app->mount('/games', include './../src/Controller/TeamGame.php');

$app->run();
