/**
 * Counter manipulation
 */
(function($){

    var Counter = function($element) {
        this.$element = $element;
    }

    Counter.prototype.score = function(s) {
        var $score = $('.score', this.$element);
        var score = parseInt($score.text());
        if (arguments.length == 0) {
            return score;
        }
        $score.text(s);
    }

    Counter.prototype.teamName = function() {
        return $('.team-name', this.$element).text();
    }

    Counter.prototype.increment = function (k) {
        this.score(this.score() + parseInt(k || 1));
    };

    Counter.prototype.decrement = function (k) {
        this.score(this.score() - parseInt(k || 1));
    };

    Counter.prototype.highlightPlayer = function (index) {
        this.clearHighlight();
        $($('.player', this.$element).get(index)).css('text-decoration', 'underline');
    }

    Counter.prototype.clearHighlight = function () {
        $('.player', this.$element).css('text-decoration', 'none');
    }

    Counter.prototype.players = function() {
        var players = [];
        $('.player', this.$element).each(function() {
            players.push($(this).text());
        })
        return players;
    }

    function Plugin (options) {
        var args = arguments;
        var ret = []
        var def =  this.each(function() {
            var data = $(this).data('counter');
            if (!data) $(this).data('counter', data = new Counter($(this)));
            if (typeof options === 'string' && options !== 'call') {
                data[options]();
            }
            if (options === 'call') {
                ret.push(data[args[1]].apply(data, Array.prototype.slice.call(args, 2)));
            }
        });

        if (options === 'call')
            return ret;
        return def;
    }

    $.fn.counter = Plugin;

    //DATA-API
    $(document).on('click.counter.data-api', '[data-counter]', function(e){
        var $target = $(this).closest('.counter');
        var method = $(this).data('counter') == 'inc' ? 'increment' : 'decrement';
        Plugin.call($target, method);
    });

}(jQuery));

/**
 * Game handling
 */
(function($){

    var Game = function($game, options) {
        this.options = options;
        this.$counters = options.counters;
        this.$game = $game;
        this.playing = {tIdx: 0, pIdx: 0, round: 0};
        this.timer = new Timer($game);
        this.abacus = new Array();

        //construct
        for (i = 0; i < this.$counters.length; i++) this.abacus[i] = 0;
        this.highlight();
    }

    Game.prototype.nextRound = function () {
        var p = this.playing;
        this.abacus[p.tIdx] = p.pIdx;
        p.tIdx = this.nextTeamIndex();
        p.pIdx = this.nextPlayerIndex();
        p.round++;
        this.$counters.counter('call', 'clearHighlight');
        $('.discard .card:last-child', this.$element).card('showName');
        $('.deck', this.$element).trigger('click');
        this.highlight(p.tIdx, p.pIdx);
        this.timer.null();
    }

    Game.prototype.pause = function () {
        this.timer.pause();
    }

    Game.prototype.highlight = function (team, player) {

        if (this.options.highlightCounters === false)
            return;

        $c = $(this.$counters[team === undefined ? this.playing.tIdx : team]);
        this.$counters.addClass('opaque');
        $c.removeClass('opaque');
        $c.counter('call', 'highlightPlayer', player === undefined ? this.playing.pIdx : player);
    }

    Game.prototype.team = function (index) {
        index = index || this.playing.tIdx;
        var $c = $(this.$counters.get(index));
        return {
            index: index,
            name:  $c.counter('call', 'teamName')[0],
            players: $c.counter('call', 'players')[0]
        }
    }

    Game.prototype.nextPlayerIndex = function() {
        var pLen = this.team().players.length;
        if (this.playing.tIdx === 0) {
            if (this.playing.round !== 0) {
                return (this.abacus[this.playing.tIdx] + 1) % pLen;
            }
        }
        return this.playing.pIdx;
    }

    Game.prototype.card = function() {
        if ($('.pile.discard').children().length === 0) return null;
        $card = $('.pile.discard .card ').last();
        return $card;
    }

    Game.prototype.roundEval = function(result) {
        var card = this.card();
        if (!card) return;
        if (result) {
            $(this.$counters[this.playing.tIdx]).counter('call', 'increment', $(card).card('get', 'toJson')[0].value);
        }
    }

    Game.prototype.play = function() {
        return this.playing;
    }

    Game.prototype.nextTeamIndex = function () {
        return (this.playing.tIdx + 1) % this.$counters.length;
    }

    function Plugin (options) {
        var defaults = {
            counters: [],
            highlightCounters: true
        }
        var settings = $.extend({}, defaults, options);
        var args = arguments;
        var ret = [];
        var def =  $(this).each(function(){
            var data = $(this).data('game');
            if (!data) $(this).data('game', data = new Game($(this), settings));
            if (options === 'call') {
                ret.push(data[args[1]].apply(data, Array.prototype.slice.call(args, 2)));
            }
        });

        if (options === 'call') return ret;
        return def;
    }

    $.fn.game = Plugin;

}(jQuery))