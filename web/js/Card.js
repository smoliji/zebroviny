var Card = Card || {};
Card.cssClass = ".card";
Card.face = {
    cssClass: ".face",
    tempHide: {
        cssClass: ".temp_hide",
        attrs: {
            hide: "data-hide-text",
            reveal: "data-reveal-text"
        }
    },
    name: {
        cssClass: ".name",
        attrs: {text: "data-name"},
        hide: function ($name) {
            $name.html($name.data('hidden-name'));
        },
        reveal: function ($name) {
            $name.html($name.attr(this.attrs.text));
        },
        isVisible: function ($name) {
            return $name.text() != $name.data('hidden-name');
        }
    }
};


$(document).on("click", Card.face.tempHide.cssClass, function(event){

    $name = $(Card.face.name.cssClass, $(this).closest(Card.face.cssClass));
    $tempHide = $(this);

    if (Card.face.name.isVisible($name)) {
       Card.face.name.hide($name);
       $tempHide.attr("title", $tempHide.attr(Card.face.tempHide.attrs.reveal));
       $("i", $tempHide).get(0).className = "fa fa-eye";
    }
    else {
       Card.face.name.reveal($name);
       $tempHide.attr("title", $tempHide.attr(Card.face.tempHide.attrs.hide));
       $("i", $tempHide).get(0).className = "fa fa-eye-slash";
    }

    event.stopImmediatePropagation();

});

/**
 * Card Manipulation Plugin
 */
(function($){

    var Card = function(element, options) {
        this.$element = $(element);
    }

    Card.prototype.hideName = function () {
        $name = $('.name', this.$element);
        $name.html($name.data('hidden-name'));
    }

    Card.prototype.showName = function () {
        $name = $('.name', this.$element);
        $name.html($name.data('name'));
    }

    Card.prototype.addNote = function (note) {
        $('.name', this.$element).after('<p>'+note+'</p>');
    }

    Card.prototype.toJson = function() {
        return {
            word: $('.name', this.$element).data('name'),
            value: $('.face', this.$element)[0].className.match(/_(\d)/)[1],
            method: $('.face', this.$element)[0].className.match(/(\w+)_\d/)[1]
        }
    }

    function Plugin (option) {
        var defaults = {}
        var settings = $.extend({}, defaults, option);
        var args = arguments;
        var ret = [];
        var def =  $(this).each(function(){
            var data = $(this).data('card');
            if (!data) $(this).data('card', data = new Card($(this), settings));
            if (option === 'get') ret.push(data[args[1]].apply(data, Array.prototype.slice.call(args, 2)));
            else if (typeof option == 'string') data[option](Array.prototype.slice.call(args, 1));
        });

        if (option === 'get') return ret;
        return def;
    }

    $.fn.card = Plugin;

})(jQuery)