$(document).ready(function () {
   $('.deck').click(function(e){
       if ($(e.target).is(".temp_hide > *") || $(e.target).is(".temp_hide")) {
           return true;
       }
       var card = $('.card', this).last();
       card.addClass('LRflipping');
       setTimeout(function () {
           card.removeClass('LRflipping');
           card.removeClass('flipped');
           card.appendTo($('.pile.discard')[0]);
       }, 2000);
       setTimeout(function () {
           $('.face', card).show();
       }, 1000);
   });
   $('.discard').click(function(e){
       if ($(e.target).is(".temp_hide > *") || $(e.target).is(".temp_hide")) {
           return true;
       }
       var card = $('.card', this).last();
       card.addClass('RLflipping');
       setTimeout(function () {
           card.removeClass('RLflipping');
           card.addClass('flipped');
           card.appendTo($('.pile.deck')[0]);
       }, 2000);
       setTimeout(function () {
           $('.face', card).hide();
       }, 1000);
   }) ;
});