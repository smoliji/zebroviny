function Timer(element) {
    this.interval = null;
    this.speed = 50;
    this.container = null;
    this.initValue = 100;
    this.render($(element));
    this.currentValue = this.initValue;
    this.reset();
}

Timer.prototype.tick = function() {
    if (this.currentValue <= 0 || this.interval !== null) return;
    var self = this;
    self.interval = setInterval(innerTick, self.speed);
    self.onTickStart();
    self.onTick();
    function innerTick() {
        self.currentValue -= 1*self.speed/1000;
        self.updateRender();
        self.onInnerTick();
        if (self.currentValue <= 0) {
            self.pause();
            self.onTimeOut();
            self.finish();
        }
    }
};

Timer.prototype.pause = function() {
    clearInterval(this.interval);
    this.interval = null;    
    this.onPause();
};

Timer.prototype.reset = function() {
    this.pause();
    this.currentValue = this.initValue;
    this.updateRender();
    this.stop();
};

Timer.prototype.null = function() {
    this.pause();
    this.currentValue = this.initValue;
    this.updateRender();
}

Timer.prototype.isPaused = function() {
    return this.interval === null;
}

Timer.prototype.toggleTick = function() {
    if (this.isPaused()) {
        this.tick();
    }
    else {
        this.pause();
    }
}

Timer.prototype.render = function(elem) {
     this.container = document.createElement('div');
    $(this.container).addClass('hourglass-mainframe');
    $(this.container).append('<div class="hourglass">\n\
                                <i class="fa fa-5x fa-play aid"></i>\n\
                                <div class="falling-sand sand"></div>\n\
                                <div class="upper sand"></div>\n\
                                <div class="lower sand"></div>\n\
                                <div class="frame"></div>\n\
                              </div>\n\
                             <div class="controls btn-group">\n\
                                 <a class="btn btn-default pause"><i class="fa fa-pause"></i></a>\n\
                                 <a class="btn btn-default run"><i class="fa fa-play"></i></a>\n\
                                 <a class="btn btn-default reset"><i class="fa fa-stop"></i></a>\n\
                                 <a class="btn btn-default countdown" ><i class="fa fa-clock-o"></i></a>\n\
                                 </div>\n\
                             <audio>\n\
                                <source src="'+path+'web/sound/boxing-bell.wav" type="audio/wav"/>\n\
                             </audio>');
    var self = this;
    $(elem).append(this.container);
    $(".countdown", this.container).click(function(){
        var newVal = prompt("Zadej nový iniciální čas stopek ve vteřinách. (> 0)", self.initValue);
        if (newVal % 1 === 0 && newVal > 0) {
            self.initValue = newVal;
        }
        self.null();
    });
    $(".pause", this.container).click(function(){self.pause();});
    $(".run", this.container).click(function(){self.tick();});
    $(".reset", this.container).click(function(){self.reset();});
    $('i.aid', this.container).hide();
    $(".hourglass", this.container).click(function(){
        self.toggleTick();
        $i = $('i.aid', this.container);
        if (self.isPaused()) {
            $i.removeClass('fa-play').addClass('fa-pause').stop().show(0).fadeOut(800);
        } else {
            $i.removeClass('fa-pause').addClass('fa-play').stop().show(0).fadeOut(800);
        }
    });
    
};

Timer.prototype.updateRender = function() {
    var step = Math.round(1000*50*this.currentValue/this.initValue)/1000;
    $('div.upper', this.container).css('height', step+'%');
    $('div.upper', this.container).css('top', 50-step+'%');
    $('div.lower', this.container).css('height', 50-step+'%');
    $('div.lower', this.container).css('top', 50+step+'%');    
    if (!this.interval) return;
    $('div.falling-sand', this.container).css('background-position-y', '+='+Math.round(step/2));
    $('div.falling-sand', this.container).css('height', step+"%");
};

Timer.prototype.onPause = function() {$('div.falling-sand', this.container).fadeOut(500);};
Timer.prototype.onTickStart = function() {$('div.falling-sand', this.container).fadeIn(500);};
Timer.prototype.onTimeOut = function() {
    var self = this;
    var hr = $('.hourglass',this.container);
    //flip&reset
    hr.addClass('justFinished');
    setTimeout(function(){self.reset();}, 600);
    setTimeout(function(){hr.removeClass('justFinished');self.reset();}, 2000);
    //ding ding ding!
    $('audio', this.container)[0].play();
};

Timer.prototype.onTick = function() {};
Timer.prototype.onInnerTick = function() {};
Timer.prototype.finish = function() {};
Timer.prototype.stop   = function() {};

