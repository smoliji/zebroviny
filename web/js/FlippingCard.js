$(document).ready(function(){    
    var card = $('.card');
    var back = $('.back', card);
    var face = $('.face', card);
    var actualBack = back;
    var next = null;
    getNextAsyn(assignNext);
    
    card.addClass('back-flip');
    //prep back side 
    setTimeout(function(){
        back.html(face.html());
        back.removeClass('back');
        back.addClass('face');
    }, 2000);
    
    card.click(function(e){
        if ($(e.target).is(".temp_hide > *") || $(e.target).is(".temp_hide")) {
            return true;
        }
        //face down
        if (!$(this).hasClass('flip')){
            $(this).removeClass('back-flip');
            $(this).addClass('flip');            
            actualBack = back;
        }
        //face up
        else {
            $(this).removeClass('flip');
            $(this).addClass('back-flip');
            actualBack = face;
        }              
        changeContent(actualBack, next);  
        getNextAsyn(assignNext);
    });    
    
    function assignNext(data) {
        next = data;        
    }
});

function getNextAsyn(cb) {
    var val;
    $.getJSON(path+'cards/random', function(data){
        var wp = data.word_phrase;
        val = [wp, data.method, data.value];
        console.log(val);
        cb(val);
    });
}

function changeContent(elem, val) {
    $('.name', elem).html($('.name').data('hidden-name'));
    elem.removeClass('say').removeClass('draw').removeClass('act');
    elem[0].className = elem[0].className.replace(/(say|draw|act)_\d/gi, '');
    elem.addClass(val[1]+'_'+val[2]);
    $(".name", elem).attr("data-name", val[0]);

}
